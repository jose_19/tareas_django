from django.db import models

# Create your models here.

class actas_nacimiento(models.Model):

    nombre = models.CharField(max_length=40)
    apellido = models.CharField(max_length=40)

    nombre_padre = models.CharField(max_length=40)
    apellido_padre = models.CharField(max_length=40)

    nombre_madre = models.CharField(max_length=40)
    apellido_madre = models.CharField(max_length=40)

    lugar_nacimiento = models.CharField(max_length=40)
    hospital_nacimiento = models.CharField(max_length=40)
    fecha_nacimiento = models.DateTimeField()
    hora_nacimiento = models.DateTimeField()

    fecha_modificacion =  models.DateTimeField()