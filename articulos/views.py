from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime

articulos = [
    {
        'nombre' : 'Generador de onda',
        'codigo' : 'G01',
        'imagen' : 'https://www.instrumentacionhoy.com/imagenes/2012/03/Generador-de-funciones-con-contador-de-seis-d%C3%ADgitos.jpg',
        'fecha' : datetime.now().strftime('%b %d %Y - %H:%M'),
    },
    {
        'nombre' : 'Osciloscopio',
        'codigo' : 'G02',
        'imagen' : 'https://i48.psgsm.net/tb.com/p/869145/480/digital-oscilloscope-siglent-sds1072cml-plus.jpg',
        'fecha' : datetime.now().strftime('%b %d %Y - %H:%M'),
    }
]

# Create your views here.
def lista_articulos(request):
    
    return render(request, "articulos.html", {'articulos':articulos}) 