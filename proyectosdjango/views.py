from django.http import HttpResponse
import datetime
import json
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render

def saludo(request):

    img = request.GET['url_img']
    nombre = request.GET['nombre']
    dpi = request.GET['dpi']

    documento= """
        <center>
            <hr width="300">
            <h2>hora: %s : %s</h2>
            <hr width="300">
            <img src="%s" width="100" height="100">
            <hr width="300">
            <h2>Nombre: %s</h2>
            <hr width="300">
            <h2>DPI: %s</h2>
            <hr width="300">
        </center>
    """ %(datetime.datetime.now().hour, datetime.datetime.now().minute, img, nombre, dpi)

    return HttpResponse(documento, content_type='text/html')