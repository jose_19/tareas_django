from django.contrib import admin
from registros.models import solicitud_visa_mexicana

# Register your models here.
@admin.register(solicitud_visa_mexicana)

class solicitud_visa_mexicanaAdmin(admin.ModelAdmin):
    list_display=("nombre", "primer_apellido", "segundo_apellido", "numero_pasaporte")