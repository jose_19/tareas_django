from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class solicitud_visa_mexicana(models.Model):
       
    usuario = models.OneToOneField(User,on_delete=models.CASCADE)

    foto=models.ImageField()
    oficina_consular=models.CharField(max_length=30)
    numero_de_folio=models.IntegerField()

    nombre=models.CharField(max_length=30)
    primer_apellido=models.CharField(max_length=30)
    segundo_apellido=models.CharField(max_length=30)
    
    GENDER_CHOICES = (
           ('M', 'Masculino'),
           ('F', 'Femenino'),
    )
    genero = models.CharField(max_length=1, choices=GENDER_CHOICES)

    fecha_nacimiento=models.DateField()
    edad=models.IntegerField()

    pais_nacimiento=models.CharField(max_length=30)
    nacionalidad=models.CharField(max_length=30)
    
    numero_pasaporte=models.IntegerField()
    pais_expedicion_pasaporte=models.CharField(max_length=30)
    fecha_expedicion_pasaporte=models.DateField()
    fecha_vencimiento_pasaporte=models.DateField()

    CIVIL_CHOICES = (
           ('S', 'Soltero(a)'),
           ('C', 'Casado(a)'),
    )
    estado_civil = models.CharField(max_length=1, choices=CIVIL_CHOICES)
    domicilio_actual=models.CharField(max_length=100)
    telefono=models.IntegerField()
    correo_electronico=models.EmailField(blank=True, null=True)
    ocupacion=models.CharField(max_length=30)  

    YERorNO_CHOICES = (
           ('S', 'Sí'),
           ('N', 'No'),
    )
    antecedentes_penales=models.CharField(max_length=1, choices=YERorNO_CHOICES)
    motivo_antecendentes=models.CharField(blank=True, null=True, max_length=50)

    fecha_ingreso_mexico=models.DateField()
    ciudad_ingreso=models.CharField(max_length=30) 

    EST_CHOICES = (
           ('Me', 'Menor a 180 días'),
           ('Ma', 'Mayor a 180 días'),
           ('De', 'Definitiva')
    )
    temporalidad_estancia=models.CharField(max_length=2, choices=EST_CHOICES)
    visitado_mexico_anteriormente=models.CharField(max_length=1, choices=YERorNO_CHOICES)
    causa_visita=models.CharField(max_length=50)
    proposito_viaje_actual=models.CharField(max_length=50)

    observaciones=models.TextField(blank=True, null=True)