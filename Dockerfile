FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /proyectosdjango
WORKDIR /proyectosdjango
COPY requirements.txt /proyectosdjango/
RUN pip install -r requirements.txt
COPY . /proyectosdjango/